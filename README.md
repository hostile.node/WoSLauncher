# Wings of Steel Launcher

![Crimson Sun launcher skin](https://www.pedro-nunes.net/external/woslauncher.png)

The **Wings of Steel Launcher** is a skinnable game launcher which downloads any required files or updates from a remote server. 

To provide an example, the launcher has been set up to download a small game (Crimson Sun) from my own server.

## Automatic updating

When the launcher is run, it will compare the game's local version with the latest version declared by a remote server (configured in WoSLauncher.xml). If the versions are different, it will download a manifest file which contains a list of all the files that should be present locally and any files that are missing or different will then be downloaded.

All the remote files are compressed with 7z and will then be decompressed after download. Decompressing can happen in parallel as files are downloading.

## Customising the launcher

The launcher can be modified by editing the *WoSLauncher.xml* file, which lets you configure the various background images, details about the remote server and text.

Obviously, since it is open source, you can create a branch and modify the launcher as needed!

## Uploading a new version to a remote server

This process can be automated by making use of [WoSPacker](https://gitlab.com/hostile.node/WoSPacker).

## Requirements

If you wish to compile the launcher from source, you'll need to get the [WoSManifestLib](https://gitlab.com/hostile.node/WoSManifestLib) as well.

## Credits

Project icon by [Freepik](https://www.freepik.com) from [FlatIcon](https://www.flaticon.com/), licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/).
