﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Windows.Forms;
using System.IO;
using WoSManifestLib;

namespace WoSLauncher
{
	public class LauncherState
	{
		public virtual void Begin(WoSLauncher launcher) { }
        public virtual void Update() { }
	}

	public class DownloadVersionState : LauncherState
	{
		public override void Begin(WoSLauncher launcher)
		{
			WebClient wc = new WebClient();
			String remoteVersion = String.Empty;
			try
			{
				remoteVersion = wc.DownloadString(launcher.ServerAddress + launcher.VersionFile);
			}
			catch (Exception)
			{
				launcher.SetState(new DownloadVersionFailedState());
			}

			launcher.RemoteVersion = remoteVersion;

            if (launcher.LocalVersion != launcher.RemoteVersion)
            {
                launcher.SetState(new DownloadManifestState());
            }
            else
            {
                launcher.SetState(new ReadyToPlayState());
            }
		}
	}

	public class DownloadManifestState : LauncherState
	{
		public override void Begin(WoSLauncher launcher)
		{
            Uri manifestUri = new Uri(launcher.ServerAddress + launcher.RemoteVersion + "/manifest.xml");
			WebClient wc = new WebClient();
			wc.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompletedCallback);
            wc.DownloadFileAsync(manifestUri, "manifest.xml");
            m_launcher = launcher;
		}

		public void DownloadFileCompletedCallback(object sender, AsyncCompletedEventArgs e)
		{
            if (e.Error == null)
            {
                m_launcher.SetState(new DownloadMissingFilesState());
            }
            else
            {
                m_launcher.SetState(new DownloadManifestFailedState());
            }
		}

        private WoSLauncher m_launcher;
	}

	public class DownloadVersionFailedState : LauncherState
	{
		public override void Begin(WoSLauncher launcher)
		{
            // Do we actually have a local version file + manifest? If not and we can't download a new one, then we likely don't
			// have a valid game...
			if (launcher.LocalVersion == "" && System.IO.File.Exists("manifest.xml") == false)
			{
				MessageBox.Show("Couldn't download version file.", "Wings of Steel launcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
				launcher.SetState(new UpdateFailedState());
			}
			else
			{
				MessageBox.Show("Couldn't download version file from remote server but the game should still be playable.", "Wings of Steel launcher", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				launcher.SetState(new ReadyToPlayState());
			}
		}
	}

	public class DownloadManifestFailedState : LauncherState
	{
		public override void Begin(WoSLauncher launcher)
		{
            MessageBox.Show("Couldn't download manifest file.", "Wings of Steel launcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
			launcher.SetState(new UpdateFailedState());
		}
	}

    public class DownloadMissingFilesState : LauncherState
    {
        public override void Begin(WoSLauncher launcher)
        {
            m_Launcher = launcher;

            Manifest localManifest = new Manifest();
            localManifest.GenerateFromDirectory(".");
            
            Manifest remoteManifest = new Manifest();
            remoteManifest.GenerateFromManifestFile("manifest.xml");

            Manifest missingFilesManifest = new Manifest();
            missingFilesManifest.GenerateFromExclusion(remoteManifest, localManifest);

            m_DownloadQueueSize = missingFilesManifest.Entries.Count;

			if (m_DownloadQueueSize == 0)
			{
				launcher.SetState(new ReadyToPlayState());
			}
			else
			{
				m_FilesDownloaded = 0;
				m_FilesDecompressed = 0;
				m_Errors = 0;

				m_DownloadQueue = new BlockingCollection<String>(m_DownloadQueueSize);
				m_DecompressionQueue = new BlockingCollection<String>(m_DownloadQueueSize);
				foreach (ManifestEntry entry in missingFilesManifest.Entries)
				{
					m_DownloadQueue.Add(entry.ShortFilename + ".wospacked");
					ThreadPool.QueueUserWorkItem(ThreadPoolDownloadCallback);
				}

				m_Launcher.ProgressBarDownload.Show();
				m_Launcher.ProgressBarDownload.Value = 0;
				m_Launcher.LabelDownload.Show();

				m_Launcher.ProgressBarDecompress.Show();
				m_Launcher.ProgressBarDecompress.Value = 0;
				m_Launcher.LabelDecompress.Show();
			}
        }

        public void ThreadPoolDownloadCallback(Object threadContext)
        {
            String downloadFilename = m_DownloadQueue.Take();
            Console.WriteLine("download thread '{0}' started...", downloadFilename);
            WebClient wc = new WebClient();

            // Make sure that the directories actually exist before downloading anything
            String downloadDirectory = "temp\\" + downloadFilename;
            downloadDirectory = downloadDirectory.Substring(0, downloadDirectory.LastIndexOf('\\'));
            System.IO.Directory.CreateDirectory(downloadDirectory);

            try
            {
                wc.DownloadFile(m_Launcher.ServerAddress + m_Launcher.RemoteVersion + "/" + downloadFilename, "temp/" + downloadFilename);
            }
            catch (WebException)
            {
                m_Errors++;
            }
            finally
            {
                m_FilesDownloaded++;
				m_DecompressionQueue.Add(downloadFilename);
				ThreadPool.QueueUserWorkItem(ThreadPoolDecompressCallback);
                Console.WriteLine("download thread '{0}' finished.", downloadFilename);
            }
        }

		public void ThreadPoolDecompressCallback(Object threadContext)
        {
            String filename = m_DecompressionQueue.Take();
            Console.WriteLine("decompress thread '{0}' started...", filename);

			ProcessStartInfo p = new ProcessStartInfo();

            p.FileName = "7za.exe";
            p.Arguments = "x \"temp\\" + filename + "\" -y";
            p.WindowStyle = ProcessWindowStyle.Hidden;

            int lastSlashIndex = filename.LastIndexOf('\\');
            if (lastSlashIndex != -1)
            {
                String outputFolder = filename.Substring(0, lastSlashIndex);
                p.Arguments += " -o\"" + outputFolder + "\"";
            }

            Process x = Process.Start(p);
	        x.WaitForExit();

            System.IO.File.Delete("temp\\" + filename);

			m_FilesDecompressed++;
            Console.WriteLine("decompress thread '{0}' finished.", filename);
        }

        public override void Update()
        {
            if (m_FilesDownloaded == m_DownloadQueueSize && m_FilesDecompressed == m_DownloadQueueSize)
            {
                if (m_Errors == 0)
                {
                    // Now that we've downloaded everything, write out a version file containing the remote version string.
                    // In subsequent runs of the launcher we won't have to download the manifest again if the versions match.
                    StreamWriter versionFile = File.CreateText("version.txt");
                    versionFile.WriteLine(m_Launcher.RemoteVersion);
                    versionFile.Close();

                    m_Launcher.SetState(new ReadyToPlayState());
                }
                else
                {
                    m_Launcher.SetState(new UpdateFailedState());
                }
            }
			
			if (m_DownloadQueueSize != 0)
			{
				int downloadPercentage = (int)((float)m_FilesDownloaded / (float)m_DownloadQueueSize * 100.0f);
				m_Launcher.ProgressBarDownload.Value = downloadPercentage;
				m_Launcher.LabelDownload.Text = String.Format("Downloading... {0}%", downloadPercentage);

				int decompressPercentage = (int)((float)m_FilesDecompressed / (float)m_DownloadQueueSize * 100.0f);
				m_Launcher.ProgressBarDecompress.Value = decompressPercentage;
				m_Launcher.LabelDecompress.Text = String.Format("Decompressing... {0}%", decompressPercentage);
			}
        }

        private BlockingCollection<String> m_DownloadQueue;
		private BlockingCollection<String> m_DecompressionQueue;
        private int m_DownloadQueueSize;
        private int m_FilesDownloaded;
		private int m_FilesDecompressed;
        private int m_Errors;
        private WoSLauncher m_Launcher;
    }

    public class UpdateFailedState : LauncherState
    {
        public override void Begin(WoSLauncher launcher)
        {
            launcher.ReadyToPlay = false;
        }
    }

    public class ReadyToPlayState : LauncherState
    {
        public override void Begin(WoSLauncher launcher)
        {
            launcher.ReadyToPlay = true;
        }
    }
}
