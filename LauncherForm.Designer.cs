﻿namespace WoSLauncher
{
    partial class WoSLauncher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WoSLauncher));
			this.labelFlavourText = new System.Windows.Forms.Label();
			this.playOverlay = new System.Windows.Forms.PictureBox();
			this.minimiseOverlay = new System.Windows.Forms.PictureBox();
			this.closeOverlay = new System.Windows.Forms.PictureBox();
			this.progressBarDownload = new System.Windows.Forms.ProgressBar();
			this.updateTimer = new System.Windows.Forms.Timer(this.components);
			this.progressBarDecompress = new System.Windows.Forms.ProgressBar();
			this.labelDownload = new System.Windows.Forms.Label();
			this.labelDecompress = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.playOverlay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.minimiseOverlay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.closeOverlay)).BeginInit();
			this.SuspendLayout();
			// 
			// labelFlavourText
			// 
			this.labelFlavourText.BackColor = System.Drawing.Color.Transparent;
			this.labelFlavourText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelFlavourText.ForeColor = System.Drawing.Color.BurlyWood;
			this.labelFlavourText.Location = new System.Drawing.Point(17, 233);
			this.labelFlavourText.Name = "labelFlavourText";
			this.labelFlavourText.Size = new System.Drawing.Size(450, 15);
			this.labelFlavourText.TabIndex = 1;
			this.labelFlavourText.Text = "<flavour text>";
			this.labelFlavourText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// playOverlay
			// 
			this.playOverlay.BackColor = System.Drawing.Color.Transparent;
			this.playOverlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.playOverlay.Cursor = System.Windows.Forms.Cursors.Hand;
			this.playOverlay.Location = new System.Drawing.Point(18, 256);
			this.playOverlay.Name = "playOverlay";
			this.playOverlay.Size = new System.Drawing.Size(450, 33);
			this.playOverlay.TabIndex = 2;
			this.playOverlay.TabStop = false;
			this.playOverlay.Click += new System.EventHandler(this.playOverlay_Click);
			this.playOverlay.MouseEnter += new System.EventHandler(this.playOverlay_MouseEnter);
			this.playOverlay.MouseLeave += new System.EventHandler(this.playOverlay_MouseLeave);
			// 
			// minimiseOverlay
			// 
			this.minimiseOverlay.BackColor = System.Drawing.Color.Transparent;
			this.minimiseOverlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.minimiseOverlay.Location = new System.Drawing.Point(927, 0);
			this.minimiseOverlay.Name = "minimiseOverlay";
			this.minimiseOverlay.Size = new System.Drawing.Size(31, 27);
			this.minimiseOverlay.TabIndex = 3;
			this.minimiseOverlay.TabStop = false;
			this.minimiseOverlay.Click += new System.EventHandler(this.minimiseOverlay_Click);
			this.minimiseOverlay.MouseEnter += new System.EventHandler(this.minimiseOverlay_MouseEnter);
			this.minimiseOverlay.MouseLeave += new System.EventHandler(this.minimiseOverlay_MouseLeave);
			// 
			// closeOverlay
			// 
			this.closeOverlay.BackColor = System.Drawing.Color.Transparent;
			this.closeOverlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.closeOverlay.Location = new System.Drawing.Point(958, 0);
			this.closeOverlay.Name = "closeOverlay";
			this.closeOverlay.Size = new System.Drawing.Size(30, 27);
			this.closeOverlay.TabIndex = 4;
			this.closeOverlay.TabStop = false;
			this.closeOverlay.Click += new System.EventHandler(this.closeOverlay_Click);
			this.closeOverlay.MouseEnter += new System.EventHandler(this.closeOverlay_MouseEnter);
			this.closeOverlay.MouseLeave += new System.EventHandler(this.closeOverlay_MouseLeave);
			// 
			// progressBarDownload
			// 
			this.progressBarDownload.ForeColor = System.Drawing.Color.Goldenrod;
			this.progressBarDownload.Location = new System.Drawing.Point(27, 97);
			this.progressBarDownload.Name = "progressBarDownload";
			this.progressBarDownload.Size = new System.Drawing.Size(239, 13);
			this.progressBarDownload.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBarDownload.TabIndex = 5;
			this.progressBarDownload.Value = 60;
			this.progressBarDownload.Visible = false;
			// 
			// updateTimer
			// 
			this.updateTimer.Enabled = true;
			this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
			// 
			// progressBarDecompress
			// 
			this.progressBarDecompress.ForeColor = System.Drawing.Color.Goldenrod;
			this.progressBarDecompress.Location = new System.Drawing.Point(27, 116);
			this.progressBarDecompress.Name = "progressBarDecompress";
			this.progressBarDecompress.Size = new System.Drawing.Size(239, 13);
			this.progressBarDecompress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBarDecompress.TabIndex = 6;
			this.progressBarDecompress.Value = 45;
			this.progressBarDecompress.Visible = false;
			// 
			// labelDownload
			// 
			this.labelDownload.BackColor = System.Drawing.Color.Transparent;
			this.labelDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelDownload.ForeColor = System.Drawing.Color.BurlyWood;
			this.labelDownload.Location = new System.Drawing.Point(272, 95);
			this.labelDownload.Name = "labelDownload";
			this.labelDownload.Size = new System.Drawing.Size(203, 15);
			this.labelDownload.TabIndex = 7;
			this.labelDownload.Text = "Downloading...";
			this.labelDownload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.labelDownload.Visible = false;
			// 
			// labelDecompress
			// 
			this.labelDecompress.BackColor = System.Drawing.Color.Transparent;
			this.labelDecompress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelDecompress.ForeColor = System.Drawing.Color.BurlyWood;
			this.labelDecompress.Location = new System.Drawing.Point(272, 114);
			this.labelDecompress.Name = "labelDecompress";
			this.labelDecompress.Size = new System.Drawing.Size(203, 15);
			this.labelDecompress.TabIndex = 8;
			this.labelDecompress.Text = "Decompressing...";
			this.labelDecompress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.labelDecompress.Visible = false;
			// 
			// WoSLauncher
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(988, 300);
			this.Controls.Add(this.labelDecompress);
			this.Controls.Add(this.labelDownload);
			this.Controls.Add(this.progressBarDecompress);
			this.Controls.Add(this.progressBarDownload);
			this.Controls.Add(this.closeOverlay);
			this.Controls.Add(this.minimiseOverlay);
			this.Controls.Add(this.playOverlay);
			this.Controls.Add(this.labelFlavourText);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "WoSLauncher";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Wings of Steel launcher";
			this.Load += new System.EventHandler(this.WoSLauncher_Load);
			((System.ComponentModel.ISupportInitialize)(this.playOverlay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.minimiseOverlay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.closeOverlay)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Label labelFlavourText;
        private System.Windows.Forms.PictureBox playOverlay;
        private System.Windows.Forms.PictureBox minimiseOverlay;
        private System.Windows.Forms.PictureBox closeOverlay;
		private System.Windows.Forms.ProgressBar progressBarDownload;
        private System.Windows.Forms.Timer updateTimer;
		private System.Windows.Forms.ProgressBar progressBarDecompress;
		private System.Windows.Forms.Label labelDownload;
		private System.Windows.Forms.Label labelDecompress;

    }
}

