﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace WoSLauncher
{
    public partial class WoSLauncher : Form
    {
        public WoSLauncher()
        {
            InitializeComponent();
			LoadPreferences();
			ReadyToPlay = false;
        }

		public void SetState(LauncherState state)
		{
            m_State = state;
            m_State.Begin(this);
		}

		public String RemoteVersion
		{
			get { return m_RemoteVersion; }
			set { m_RemoteVersion = value; }
		}

        public String LocalVersion
        {
            get { return m_LocalVersion; }
        }

        public String VersionFile
        {
            get { return m_VersionFile; }
        }

        public String ServerAddress
        {
            get { return m_ServerAddress; }
        }

		public ProgressBar ProgressBarDownload
		{
			get { return progressBarDownload; }
		}	

		public ProgressBar ProgressBarDecompress
		{
			get { return progressBarDecompress; }
		}

		public Label LabelDownload
		{
			get { return labelDownload; }
		}

		public Label LabelDecompress
		{
			get { return labelDecompress; }
		}

		public bool ReadyToPlay
		{
			get { return m_ReadyToPlay; }
			set { 
				m_ReadyToPlay = value; 
				playOverlay.Image = value ? m_PlayImage : m_PlayImageDisabled;
			}
		}

		private void WoSLauncher_Load(object sender, EventArgs e)
		{
			this.BackgroundImage = m_BackgroundImage;

            ReadLocalVersion();
            SetState(new DownloadVersionState());
		}

        private void playOverlay_MouseEnter(object sender, EventArgs e)
        {
			if (m_ReadyToPlay)
			{
				playOverlay.BackgroundImage = m_PlayImageHighlighted;
			}
        }

        private void playOverlay_MouseLeave(object sender, EventArgs e)
        {
			if (m_ReadyToPlay)
			{
				playOverlay.BackgroundImage = m_PlayImage;
			}
        }

        private void minimiseOverlay_MouseEnter(object sender, EventArgs e)
        {
            minimiseOverlay.BackgroundImage = m_MinimiseOverlayImage;
        }

        private void minimiseOverlay_MouseLeave(object sender, EventArgs e)
        {
            minimiseOverlay.BackgroundImage = null;
        }

        private void closeOverlay_MouseEnter(object sender, EventArgs e)
        {
            closeOverlay.BackgroundImage = m_CloseOverlayImage;
        }

        private void closeOverlay_MouseLeave(object sender, EventArgs e)
        {
            closeOverlay.BackgroundImage = null;
        }

        private void minimiseOverlay_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void closeOverlay_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void playOverlay_Click(object sender, EventArgs e)
        {
			if (this.ReadyToPlay)
			{
				Process process = new Process();
				process.StartInfo.FileName = m_Executable;
				process.Start();

				Close();
			}
        }

		private void LoadPreferences()
        {
            if (System.IO.File.Exists("WoSLauncher.xml"))
            {
                XmlReader reader = XmlReader.Create("WoSLauncher.xml");
                while (reader.Read())
                {
                    if (reader.IsStartElement() && !reader.IsEmptyElement)
                    {
						if (reader.Name == "Title")
						{
							reader.Read();
							this.Text = reader.Value;
						}
						else if (reader.Name == "VersionFile")
						{
							reader.Read();
							m_VersionFile = reader.Value;
						}
                        else if (reader.Name == "FlavourText")
                        {
                            reader.Read();
                            labelFlavourText.Text = reader.Value;
                        }
                        else if (reader.Name == "ServerAddress")
                        {
                            reader.Read();
                            m_ServerAddress = reader.Value;

                            // Sanitise address so that it ends with a /
                            if (m_ServerAddress.EndsWith("/") == false)
                            {
                                m_ServerAddress += "/";
                            }
                        }
                        else if (reader.Name == "BackgroundImage")
                        {
                            reader.Read();
							m_BackgroundImage = LoadImage(reader.Value);
                        }
                        else if (reader.Name == "MinimiseOverlayImage")
                        {
                            reader.Read();
							m_MinimiseOverlayImage = LoadImage(reader.Value);
                        }
                        else if (reader.Name == "CloseOverlayImage")
                        {
                            reader.Read();
							m_CloseOverlayImage = LoadImage(reader.Value);
                        }
						else if (reader.Name == "PlayImage")
                        {
                            reader.Read();
							m_PlayImage = LoadImage(reader.Value);
                        }
                        else if (reader.Name == "PlayImageHighlighted")
                        {
                            reader.Read();
							m_PlayImageHighlighted = LoadImage(reader.Value);
                        }
						else if (reader.Name == "PlayImageDisabled")
                        {
                            reader.Read();
							m_PlayImageDisabled = LoadImage(reader.Value);
                        }
						else if (reader.Name == "Executable")
						{
							reader.Read();
							m_Executable = reader.Value;
						}
                    }
                }
                reader.Close();
            }
        }

		private Image LoadImage(String filename)
		{
			if (System.IO.File.Exists(filename))
            {
                return Image.FromFile(filename);
            }
            else
            {
                MessageBox.Show(String.Format("Couldn't find image: {0}", filename), "Wings of Steel launcher", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return null;
            }
		}

        private void ReadLocalVersion()
        {
            try
            {
                StreamReader sr = new StreamReader(m_VersionFile);
                m_LocalVersion = sr.ReadLine();
                sr.Close();
            }
            catch (Exception)
            {

            }
        }

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            if (m_State != null)
            {
                m_State.Update();
            }
        }

		Image m_BackgroundImage;
		Image m_MinimiseOverlayImage;
		Image m_CloseOverlayImage;
		Image m_PlayImage;
		Image m_PlayImageHighlighted;
		Image m_PlayImageDisabled;
		String m_RemoteVersion;
        String m_LocalVersion;
        String m_VersionFile;
        String m_ServerAddress;
        LauncherState m_State;
		bool m_ReadyToPlay;
		String m_Executable;
    }
}
